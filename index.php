<?php
//require '/assets/aws-sdk/aws-autoloader.php'



//get instance ID
$url = "http://169.254.169.254/latest/meta-data/instance-id";
$inst_id = file_get_contents($url);

//get AZ
$url = "http://169.254.169.254/latest/meta-data/placement/availability-zone";
$zone = file_get_contents($url);

//get security group
$url = "http://169.254.169.254/latest/meta-data/security-groups";
$sec_grp = file_get_contents($url);

//get public hostname
$url = "http://169.254.169.254/latest/meta-data/public-hostname";
$pub_hostname = file_get_contents($url);

$filename = '/var/www/html/stress/cputestrunning.txt';
if (file_exists($filename)){
    $test_action = "stop";
    $test_button = "Stop Test";
}else{
    $test_action = "start";
    $test_button = "Start Test";
}

$cpuload = sys_getloadavg();

?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>AWS Autoscaling Web App</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="Description" lang="en" content="Test Site for AWS Stuff">
		<meta name="author" content="Laurence Davenport">
		<meta name="robots" content="index, follow">

		<!-- icons -->
		<link rel="apple-touch-icon" href="assets/img/apple-touch-icon.png">
		<link rel="shortcut icon" href="favicon.ico">

		<!-- Override CSS file - add your own CSS rules -->
		<link rel="stylesheet" href="assets/css/styles.css">
	</head>
    <body>
		<div class="header">
			<div class="container">
				<h1 class="header-heading">AWS Autoscaling</h1>
			</div>
		</div>
		<div class="nav-bar">
			<div class="container">
				<a href="mailto:ldaven@amazon.com">ldaven@amazon.com</a>
			</div>
		</div>
        <div class="content">
			<div class="container">
				<div class="main">
					<h1>EC2 Instance</h1>
                    <h4><?php print $pub_hostname ; ?></h4>
					<hr>

					<!-- Paragraphs -->
					<p>This is the EC2 instance used to generate this page</p>
                    <p> EC2 instance ID: <?php print $inst_id ; ?> </p>
                    <p> Avalability Zone: <?php print $zone ; ?> </p>
                    <p> Security Group: <?php print $sec_grp ; ?> </p>
                    <hr />
                    <img src="https://s3-eu-west-1.amazonaws.com/autoscalingapp-s3/AWS_Logo_PoweredBy_300px_0.jpg">
                    
            </div>
            
            <div class="aside">
					<h3>Dynamic Content</h3>
					   <p>Information from RDS</p>
                       
                            <?php include 'rds.disp.php'; ?>
                        <hr />
                        <?php
                        print "<p> The current CPU load is: $cpuload[0]% </p>";
                        print "<p> To control the CPU load test use the button below: </p>";
                        print "<p><a href=\"stresscpu.php?status=$test_action\" class=\"btn\">$test_button</a></p>";
                        ?>
                </div>
		  </div>
        </div>    
            <div class="footer">
			<div class="container">
				&copy; Copyright 2015
			</div>
		</div>
	</body>
</html>